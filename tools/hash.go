package tools

import (
	"golang.org/x/crypto/bcrypt"
)

func Hash(pwd string) (string) {
	hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.MinCost)
	if err != nil {
		panic(err)
	}

	return string(hash)
}

func CheckHash(hashedPwd string, plainPwd string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(plainPwd))

	return err == nil
}
