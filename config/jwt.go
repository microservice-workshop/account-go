package config

import (
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type claim struct {
	UserID   string `json:"user_id"`
	Username string `json:"username"`
	jwt.StandardClaims
}

func TokenGenerate(userID int, username string) (string, error) {

	var jwtKey = []byte("5935CA443CB862ADACFA1E826AC51")
	var expiredTime = time.Now().AddDate(0, 1, 0).UnixNano() / int64(time.Millisecond)

	strUserID := strconv.Itoa(userID)
	jwtClaim := claim{
		UserID:   strUserID,
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiredTime,
			Issuer:    "louisaldorio",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwtClaim)

	signedToken, err := token.SignedString(jwtKey)

	return signedToken, err
}
