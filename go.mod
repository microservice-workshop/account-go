module myapp

go 1.15

require (
	github.com/99designs/gqlgen v0.14.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.4
	github.com/rs/cors v1.6.0
	github.com/vektah/gqlparser v1.3.1
	github.com/vektah/gqlparser/v2 v2.2.0
	gitlab.com/microservice-workshop/utils-go.git v1.0.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.22.3
)
