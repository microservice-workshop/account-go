package service

import (
	"myapp/config"
	"myapp/graph/model"
	"myapp/tools"

	"github.com/vektah/gqlparser/v2/gqlerror"
	"gorm.io/gorm"
)

func Register(input model.NewUser) (string, error) {

	db := config.GetDB()

	newUser := model.User{
		Username:       input.Username,
		HashedPassword: tools.Hash(input.Password),
	}

	var user model.User
	err := db.Model(&model.User{}).Where("username = ?", input.Username).Take(&user).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			err := db.Model(&model.User{}).Create(&newUser).Error
			if err != nil {
				panic(err)
			}

			token, err := config.TokenGenerate(newUser.ID, newUser.Username)
			if err != nil {
				panic(err)
			}

			return token, nil
		} else {
			panic(err)
		}
	}

	return "", gqlerror.Errorf("Username is taken!")
}